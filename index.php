    <h1>libphonenumber Demo</h1>
    <form class="form-horizontal" method="get" action="">
        <div class="form-group">
            <label for="phonenumber" class="col-sm-3 control-label">Phone Number</label>

            <div class="col-sm-5">
                <input type="tel" class="form-control" id="phonenumber" name='phonenumber' required="required"
                       placeholder="0161 496 0000">
            </div>
        </div>
        <div class="form-group">
            <label for="country" class="col-sm-3 control-label">Default Country</label>

            <div class="col-sm-3">
                <input type="text" class="form-control" id="country" name='country' required="required"
                       placeholder="GB">
                <span class="help-block">An <a href='http://www.iso.org/iso/country_names_and_code_elements'>ISO
                        3166-1</a> two letter country code</span>
            </div>
        </div>
        <div class="form-group">
            <label for="language" class="col-sm-3 control-label">Locale for Geocoding</label>

            <div class="col-sm-3">
                <div class="form-group">
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="language" name='language' placeholder="en">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="region" name='region' placeholder="GB">
                    </div>
                </div>
                <span class="help-block">An <a href='http://en.wikipedia.org/wiki/IETF_language_tag'>IETF language
                        tag</a> (defaults to en)</span>
            </div>

        </div>
        <div class="form-group">
            <div class="col-lg-offset-3 col-sm-8">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
<?php

require __DIR__ . '/vendor/autoload.php';

/* Check if we have loaded variables */
if (isset($_GET['phonenumber']) && $_GET['phonenumber'] != '' && isset($_GET['country']) && $_GET['country'] != '') {
echo "<pre>";
    $input = array();
    $input['phonenumber'] = $_GET['phonenumber'];
    $input['country'] = $_GET['country'];
    $input['language'] = (isset($_GET['language']) && $_GET['language'] != '') ? $_GET['language'] : 'en';
    $input['region'] = (isset($_GET['region']) && $_GET['region'] != '') ? $_GET['region'] : null;


    $phoneNumberUtil = \libphonenumber\PhoneNumberUtil::getInstance();
    $shortNumberInfo = \libphonenumber\ShortNumberInfo::getInstance();
    $phoneNumberGeocoder = \libphonenumber\geocoding\PhoneNumberOfflineGeocoder::getInstance();

    $validNumber = false;
    $validNumberForRegion = false;
    $possibleNumber = false;
    $isPossibleNumberWithReason = null;
    $geolocation = null;
    $phoneNumberToCarrierInfo = null;
    $timezone = null;

    try {
        $phoneNumber = $phoneNumberUtil->parse($input['phonenumber'], $input['country'], null, true);
        $possibleNumber = $phoneNumberUtil->isPossibleNumber($phoneNumber);
        $isPossibleNumberWithReason = $phoneNumberUtil->isPossibleNumberWithReason($phoneNumber);
        $validNumber = $phoneNumberUtil->isValidNumber($phoneNumber);
        $validNumberForRegion = $phoneNumberUtil->isValidNumberForRegion($phoneNumber, $input['country']);
        $phoneNumberRegion = $phoneNumberUtil->getRegionCodeForNumber($phoneNumber);
        $phoneNumberType = $phoneNumberUtil->getNumberType($phoneNumber);

        $geolocation = $phoneNumberGeocoder->getDescriptionForNumber(
            $phoneNumber,
            $input['language'],
            $input['region']
        );

        $phoneNumberToCarrierInfo = \libphonenumber\PhoneNumberToCarrierMapper::getInstance()->getNameForNumber(
            $phoneNumber,
            $input['language']
        );
        $timezone = \libphonenumber\PhoneNumberToTimeZonesMapper::getInstance()->getTimeZonesForNumber($phoneNumber);

    } catch (\Exception $e) {
            var_dump(array(
                'input' => $input,
                'exception' => $e,
            )
        );
        exit;
    }

    $regions = $phoneNumberUtil->getSupportedRegions();

    asort($regions);

    $baseLanguagePath = __DIR__ . '/vendor/umpirsky/country-list/country/cldr/';

    $resolvedPath = realpath($baseLanguagePath . $input['language'] . '/country.php');
    if (strpos($resolvedPath, $baseLanguagePath) === 0 && file_exists($resolvedPath)) {
        $countries = require $resolvedPath;
    } else {
        $countries = require $baseLanguagePath . 'en/country.php';
    }

        var_dump(array(
            'phoneNumberObj' => $phoneNumber,
            //'phoneUtil' => $phoneNumberUtil,
            'possibleNumber' => $possibleNumber,
            'isPossibleNumberWithReason' => $isPossibleNumberWithReason,
            'validNumber' => $validNumber,
            'validNumberForRegion' => $validNumberForRegion,
            'phoneNumberRegion' => $phoneNumberRegion,
            'phoneNumberType' => $phoneNumberType,
            'geolocation' => $geolocation,
            'timezone' => $timezone,
            'carrierinfo' => $phoneNumberToCarrierInfo,
            //'shortNumber' => $shortNumberInfo,
            'input' => $input,
            //'regions' => $regions,
            //'countries' => $countries,
        )
    );
} else {
}
?>
